#pragma once

#include <exception>
#include <string>

class dlexception : public std::exception
{
public:
	virtual const char* what() const throw();
};

class dlopenexception : public dlexception
{
public:
	dlopenexception(std::string lib_name, std::string bonus = "");
	virtual const char* what() const throw();
private:
	std::string m_msg;
};

class dlsymexception : public dlexception
{
public:
	dlsymexception(std::string msg, std::string bonus = "");
	virtual const char* what() const throw();
private:
	std::string m_msg;
};