#pragma once

#include "Session.hpp"

class SessionI3 : public Session {
public:
	SessionI3(boost::asio::local::stream_protocol::socket sock);

	void start();

protected:
	virtual void do_read();

	virtual void do_write(std::string msg);

private:
	SessionI3() = delete;
	SessionI3(Session const&) = delete;
    SessionI3& operator=(SessionI3 const&) = delete;
};