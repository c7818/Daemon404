#pragma once

#include <boost/program_options.hpp>
#include <string>

class ModuleInterface
{
public:
	ModuleInterface(){}
	virtual ~ModuleInterface(){}

	virtual boost::program_options::options_description getOptions() {
		return boost::program_options::options_description();
	}

	virtual void handleOptions(const boost::program_options::variables_map& vm) {}

	virtual std::string handleMessage(std::string msg) {
		return "Unimplemented option";
	}

private:
	ModuleInterface(ModuleInterface const&) = delete;
    ModuleInterface& operator=(ModuleInterface const&) = delete;
};

typedef ModuleInterface *create_t();
typedef void destroy_t(ModuleInterface*);