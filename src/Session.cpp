#include "Session.hpp"
#include "Daemon.hpp"

using boost::asio::local::stream_protocol;

Session::Session(stream_protocol::socket sock)
: m_socket(std::move(sock))
{
}

void Session::start()
{
	do_read();
}

void Session::do_read()
{
	auto self(shared_from_this());
	m_socket.async_read_some(boost::asio::buffer(m_data),
		[this, self](boost::system::error_code ec, std::size_t length)
		{
			if (!ec){
				m_data[length] = '\0';
				do_write(std::move(Daemon::Instance()->handleMessage(m_data, length)));
			}
		});
}

void Session::do_write(std::string msg)
{
	auto self(shared_from_this());
	boost::asio::async_write(m_socket,
		boost::asio::buffer(&msg[0], msg.size()),
        [this, self](boost::system::error_code ec, std::size_t /*length*/)
		{
			if (!ec)
				do_read();
		});
}