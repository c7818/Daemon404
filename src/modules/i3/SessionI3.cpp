#include "Daemon.hpp"
#include "i3/ModuleI3.hpp"
#include "i3/SessionI3.hpp"

using boost::asio::local::stream_protocol;

SessionI3::SessionI3(stream_protocol::socket sock)
: Session(std::move(sock))
{
}

void SessionI3::start()
{
	do_read();
}

void SessionI3::do_read()
{
	auto self(shared_from_this());
	m_socket.async_read_some(boost::asio::buffer(m_data),
		[this, self](boost::system::error_code ec, std::size_t length)
		{
			if (!ec){
				m_data[length] = '\0';
				static_cast<ModuleI3*>(Daemon::Instance()->getModule("i3")->get())->handleI3(m_data, length);
			}
		});
}

void SessionI3::do_write(std::string msg)
{
	
}