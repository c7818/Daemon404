#include <iostream>
#include <csignal>
#include <cstdlib>
#include <sstream>
#include <string>
#include <vector>

#include "Config.hpp"
#include "Daemon.hpp"
#include "Server.hpp"
#include "Session.hpp"

Daemon *Daemon::instance = NULL;

Daemon::Daemon(){

}

Daemon::~Daemon(){

}

Daemon *Daemon::Instance(){
	if (instance == NULL)
    {
        instance = new Daemon();
    }
    return instance;
}

int Daemon::start(){
	// Declare the supported options.
	try {
		signal(SIGINT, [](int signum){
			Daemon::Instance()->stop();
		});

		if (getenv("XDG_RUNTIME_DIR") == NULL) {
			std::cerr << "error: cannot find XDG_RUNTIME_DIR" << std::endl;
			return 1;
		}
		std::string runtime_dir = getenv("XDG_RUNTIME_DIR");
		runtime_dir += "/daemon404.socket";

		std::remove(runtime_dir.c_str());
		Server<Session> s1(m_io_service, runtime_dir);

		m_io_service.run();
	}
	catch(std::exception& e) {
		std::cerr << "error: " << e.what() << std::endl;
		std::cerr << "Use --help to get info on how to use daemon404." << std::endl;
		return 1;
	}

	return 0;
}

void Daemon::stop(){
	m_io_service.stop();
	m_modules.clear();
}

std::string Daemon::handleMessage(const std::array<char, 1024> &data, std::size_t length){
	std::istringstream input(data.data());
	std::string token;
	if (!(input >> token))
		return "Failure: no command";

	std::string ret = "Success";
	try {
		if (token == "stop" && input.eof()) {
			stop();
		} else {
			if (m_modules.find(token) == m_modules.end())
				ret = "Failure: unknown module";
			else {
				std::string remains;
				std::getline(input, remains);
				ret = m_modules.find(token)->second.handleMessage(remains);
			}
		}
	} catch(std::exception& e) {
		ret = std::string("error") + e.what();
	}
	return ret;
}

void Daemon::addModule(std::string module_name) {
	m_modules.emplace(module_name, std::string("libdaemon404-") + module_name + std::string(".so"));
}

boost::program_options::options_description Daemon::getOptions() {
	boost::program_options::options_description desc;

	for (auto& m : m_modules) {
    	desc.add(m.second.getOptions());
	}

	return desc;
}

void Daemon::handleOptions(const boost::program_options::variables_map& vm) {
	for (auto& m : m_modules) {
    	m.second.handleOptions(vm);
	}
}

Module *Daemon::getModule(std::string name) {
	if (m_modules.find(name) == m_modules.end())
		return &m_modules.find(name)->second;
	else
		return nullptr;
}